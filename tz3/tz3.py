from random import randrange


def find_min(array: list) -> int:
    minimum = None
    for i in array:
        if minimum is None:
            minimum = i
        if i < minimum:
            minimum = i
    return minimum


def find_max(array: list) -> int:
    maximum = None
    for i in array:
        if maximum is None:
            maximum = i
        if maximum < i:
            maximum = i
    return maximum


def find_sum(array: list) -> int:
    summ = None
    for i in array:
        if summ is None:
            summ = i
        else:
            summ += i
    return summ


def find_product(array: list) -> int:
    product = None
    for i in array:
        if product is None:
            product = i
        else:
            product *= i
    return product


def create_rand_array(amount, low, high) -> list:
    array = list()
    for i in range(amount):
        array.append(randrange(low, high))
    return array


def read_file(filename):
    with open(filename) as file:
        line = file.readline()
    numbers_list = list(map(int, line.split(' ')))
    return numbers_list


name_of_file = 'timetest8.txt'
if __name__ == "__main__":
    num_list = read_file(name_of_file)
    try:
        minimum = find_min(num_list)
        print(f'minimum: {minimum}')
    except OverflowError:
        print("Wow! Overflow Error occurred while calculating minimum")

    try:
        maximum = find_max(num_list)
        print(f'maximum: {maximum}')
    except OverflowError:
        print("Wow! Overflow Error occurred while calculating maximum")

    try:
        summary = find_sum(num_list)
        print(f'sum: {summary}')

    except OverflowError:
        print("Wow! Overflow Error occurred while calculating sum")

    try:
        product = find_product(num_list)
        print(f'product: {product}')
    except OverflowError:
        print("Wow! Overflow Error occurred while calculating product")



