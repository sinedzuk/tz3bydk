from tz3 import find_min, find_max, find_sum, find_product, create_rand_array, name_of_file, read_file
import math, time

randarray = create_rand_array(1000, 1, 11)


def test_min():
    assert min(randarray) == find_min(randarray)


def test_max():
    assert max(randarray) == find_max(randarray)


def test_sum():
    assert sum(randarray) == find_sum(randarray)


def test_prod():
    assert math.prod(randarray) == find_product(randarray)
    # assert False


def test_time_dependency():

    a = create_rand_array(10000, 1, 11)
    start_time1 = time.time()
    find_min(a)
    find_max(a)
    find_sum(a)
    find_product(a)
    total_time1 = time.time() - start_time1
    b = create_rand_array(100000, 1, 11)
    start_time2 = time.time()
    find_min(b)
    find_max(b)
    find_sum(b)
    find_product(b)
    total_time2 = time.time() - start_time2
    assert total_time1 < total_time2


def test_no_negative_values_in_file():
    assert min(read_file(name_of_file)) >= 0

